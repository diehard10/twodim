﻿using System;
using MvvmCross.Binding.Droid.Views;
using Android.Content;
using MvvmCross.Binding.Droid.BindingContext;
using Android.Views;
using System.Collections.Generic;
using System.Collections;
using TwoDim.Droid.TpsCarousel.BindableCarousel;

namespace TwoDim.Droid.TpsCarousel.TpsCarousel.Adapters
{
	public class TpsCarouselAdapter : MvxAdapter
	{
		public TpsCarouselAdapter (Context context, IMvxAndroidBindingContext bindingContext) : base (context, bindingContext) {}
		public TpsCarouselAdapter (Context context) : base (context) {}

		protected override View GetBindableView (View convertView, object dataContext, int templateId)
		{
			convertView = base.GetBindableView (convertView, dataContext, templateId);
			convertView.FindViewById <BindableHorizontalListView> (Resource.Id.innerItem).ItemsSource = (IEnumerable) dataContext;

			return convertView;
		}
	}
}

