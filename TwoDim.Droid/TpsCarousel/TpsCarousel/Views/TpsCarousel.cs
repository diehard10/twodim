﻿using System;
using MvvmCross.Binding.Droid.Views;
using Android.Content;
using Android.Util;
using TwoDim.Droid.TpsCarousel.TpsCarousel.Adapters;

namespace TwoDim.Droid.TpsCarousel.TpsCarousel.Views
{
	public class TpsCarousel : MvxListView
	{
		public TpsCarousel (Context context, IAttributeSet attrs, IMvxAdapter adapter) : base (context, attrs, new TpsCarouselAdapter (context)) {}
		public TpsCarousel (Context context, IAttributeSet attrs) : base (context, attrs) {}
	}
}

