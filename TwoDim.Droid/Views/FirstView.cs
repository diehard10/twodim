using Android.App;
using Android.OS;
using Android.Views;
using Android.Graphics;
using Android.Widget;
using Android.Media;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using Android.Content;
using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Views;
using TwoDim.Droid.TpsCarousel.TpsCarousel.Adapters;
using MvvmCross.Binding.Droid.BindingContext;
using Android;

namespace TwoDim.Droid.Views
{
	[Activity(Label = "FirstView", MainLauncher = true)]
	public class FirstView : MvxActivity
	{
		private TwoDim.Droid.TpsCarousel.TpsCarousel.Views.TpsCarousel list;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			SetContentView (Resource.Layout.FirstView);
			Init ();
		}

		private void Init ()
		{
			FindViewReferences ();
			CustomiseViews ();
		}

		private void FindViewReferences ()
		{
			list = FindViewById <TwoDim.Droid.TpsCarousel.TpsCarousel.Views.TpsCarousel> (Resource.Id.mainLayout);
		}

		private void CustomiseViews ()
		{
			list.Adapter = new TpsCarouselAdapter (this, (IMvxAndroidBindingContext)BindingContext);
		}
	}
}