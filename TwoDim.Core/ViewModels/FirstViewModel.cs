using System.Collections.Generic;
using MvvmCross.Core.ViewModels;

namespace TwoDim.Core.ViewModels
{
	public class FirstViewModel	: MvxViewModel
	{
		private List <string> theList;
		public List <string> TheList
		{
			get { return theList; }
			set { theList = value; RaisePropertyChanged (() => TheList); }
		}

		private List <List <string>> the2DList;
		public List <List <string>> The2DList
		{
			get { return the2DList; }
			set {
				the2DList = value;
				RaisePropertyChanged (() => The2DList);
			}
		}

		public FirstViewModel ()
		{
			The2DList = new List <List <string>> ();

			for (var i = 0; i < 4; i++) 
				The2DList.Add (new List <string> () {
					i + " one",
					i + "two",
					i + "three",
					i + "four"
				});
		}
	}
}
